package com.epam;

import java.util.Scanner;

/**
 * This class allows to user to enter interval of numbers. Then odd and
 * even numbers are printed by program.
 * Next step is printing sum of all odd numbers and
 * then sum of all even numbers.
 *
 * @version 1.8 08 Nov 2019
 * @author Vitalii Yaremko
 */

public class Foo {
    public static void main(final String[] args) {
        int beginOfInterval = enterBeginOfInterval();
        int endOfInterval = enterEndOfInterval();
        printOddNumbers(beginOfInterval, endOfInterval);
        printEvenNumbers(beginOfInterval, endOfInterval);
    }

    private static void Foo(){};

    static private int enterBeginOfInterval() {
        System.out.println("Enter begin of interval:");
        Scanner scanner = new Scanner(System.in);
        int beginOfInterval = scanner.nextInt();
        return beginOfInterval;
    }

    static private int enterEndOfInterval() {
        System.out.println("Enter end of interval:");
        Scanner scanner = new Scanner(System.in);
        int endOfInterval = scanner.nextInt();
        return endOfInterval;
    }

    static private void printOddNumbers(int beginOfInterval,
                                       int endOfInterval) {
        int sumOfOddNumbers = 0;
        System.out.println("All odd numbers in interval ["
                + beginOfInterval + ";" + endOfInterval + "] :");
        for (int i = beginOfInterval; i <= endOfInterval; i++) {
            if (i % 2 != 0) {
                System.out.println(i);
                sumOfOddNumbers+=i;
            }
        }
        System.out.println("Sum of odd numbers = " + sumOfOddNumbers);
    }

    static private void printEvenNumbers(int beginOfInterval,
                                        int endOfInterval) {
        int sumOfEvenNumbers = 0;
        System.out.println("All even numbers in interval ["
                + beginOfInterval + ";" + endOfInterval + "] :");
        for (int i = beginOfInterval; i <= endOfInterval; i++) {
            if (i % 2 != 1) {
                System.out.println(i);
                sumOfEvenNumbers += i;
            }
        }
        System.out.println("Sum of even numbers = "
                + sumOfEvenNumbers);
    }

}
