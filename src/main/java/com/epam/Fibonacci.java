package com.epam;

import java.util.Scanner;

public class Fibonacci {
    public static void main(final String[] args) {

        int sizeOfFibonacciSet = enterSizeOfFibonacciSet();
        printBiggestOddAndEvenNumbers(sizeOfFibonacciSet);
    }

    private static void Fibonacci(){};

    static private int enterSizeOfFibonacciSet() {
        System.out.println("Enter size of Fibonacci set:");
        Scanner scanner = new Scanner(System.in);
        final int sizeOfFibonacciSet = scanner.nextInt();
        return sizeOfFibonacciSet;
    }

    static private void printBiggestOddAndEvenNumbers(int sizeOfFibonacciSet) {
        int size = 2;
        int maxOddNumber = 0;
        int maxEvenNumber = 0;
        int[] fibonacciArray;
        fibonacciArray = new int[sizeOfFibonacciSet];
        if (sizeOfFibonacciSet >= 1) {
            fibonacciArray[0] = 1;
        }
        if (sizeOfFibonacciSet >= 2) {
            fibonacciArray[1] = 1;
        }
        while (size < sizeOfFibonacciSet) {
            fibonacciArray[size] = fibonacciArray[size - 2]
                    + fibonacciArray[size - 1];
            if (fibonacciArray[size] % 2 == 0) {
                maxEvenNumber = fibonacciArray[size];
            } else {
                maxOddNumber = fibonacciArray[size];
            }
            size++;
        }
        final int constant = 3;
        if (sizeOfFibonacciSet == 0) {
            System.out.println("There no odd number in Fibonacci set!");
            System.out.println("There no even number in Fibonacci set!");
        } else if ((sizeOfFibonacciSet == 1) || (sizeOfFibonacciSet == 2)) {
            System.out.println("The biggest odd number in Fibonacci set is 1");
            System.out.println("There no even number in Fibonacci set!");
        } else if (sizeOfFibonacciSet == constant) {
            System.out.println("The biggest even number in Fibonacci set is 2");
            System.out.println("The biggest odd number in Fibonacci set is 1");
        } else if (sizeOfFibonacciSet > constant) {
            System.out.println("The biggest even number in Fibonacci set is "
                    + maxEvenNumber);
            System.out.println("The biggest odd number in Fibonacci set is "
                    + maxOddNumber);
        }
        if (sizeOfFibonacciSet != 0) {
            printPercentageOfOddAndEvenNumbers (
                     fibonacciArray, sizeOfFibonacciSet);
        }

    }

    static private void printPercentageOfOddAndEvenNumbers (
            final int fibonacciArray[], final int sizeOfFibonacciSet) {
        int amountOfOddNumbers = 0;
        int amountOfEvenNumbers = 0;
        for (int counter = 0; counter < sizeOfFibonacciSet; counter++) {
            if (fibonacciArray[counter] % 2 == 0) {
                amountOfEvenNumbers++;
            } else {
                amountOfOddNumbers++;
            }
        }
        final int percentageKoef = 100;
        System.out.println("Percentage of odd numbers is "
                + (double) amountOfOddNumbers / sizeOfFibonacciSet
                * percentageKoef + "%");
        System.out.println("Percentage of even numbers is "
                + (double) amountOfEvenNumbers / sizeOfFibonacciSet
                * percentageKoef + "%");
    }
}
